#!/bin/bash

set -eu

# Setting up CouchPotato data dir
mkdir -p /app/data/couchpotato

mkdir -p /app/data/Movies

chown -R cloudron:cloudron /app/data /tmp /run

echo "Starting SickChill..."
/usr/local/bin/gosu cloudron:cloudron python2.7 /app/code/couchpotato/CouchPotato.py --data_dir=/app/data/couchpotato --console_log
